import client from '../utils/HttpClient'
import * as protobuf from 'protobufjs'
import * as path from 'path'

export default class RealtimeGtfsProto {
    private static instance: RealtimeGtfsProto
    private static protoFile = path.join(__dirname, '../../proto/gtfs-realtime.proto')
    private static msgType = 'transit_realtime.FeedMessage'
    private message: protobuf.Type

    private constructor() {}

    private async loadProto(): Promise<protobuf.Root> {
        let root: protobuf.Root

        try {
            root = await protobuf.load(RealtimeGtfsProto.protoFile)
        } catch (e) {
            throw e
        }

        return root
    }

    static getInstance() {
        if (!RealtimeGtfsProto.instance) {
            RealtimeGtfsProto.instance = new RealtimeGtfsProto()

            let tmpRoot = RealtimeGtfsProto.instance.loadProto()
            tmpRoot.then(proto => {
                RealtimeGtfsProto.instance.message = proto.lookupType(RealtimeGtfsProto.msgType)
            })
            .catch(err => {
                throw err
            })
        }

        return RealtimeGtfsProto.instance
    }

    async fetchRealtimeData<T>(url: string): Promise<T|{}> {
        try {
            let resp = await client.getContentBuffer(url)
            let msg = this.message.decode(resp)

            return msg

        } catch (e) {
            throw e
        }
    }
}
