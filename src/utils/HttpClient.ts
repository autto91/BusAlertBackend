module client {
    export function getContentBuffer(url: string): Promise<Buffer> {
        return new Promise((resolve, reject) => {
            // TODO: clean this up
            const lib = url.startsWith('https') ? require('https') : require('http')
            const req = lib.get(url, resp => {
                if (resp.statusCode < 200 || resp.statusCode > 299)
                    reject(new Error(`Failed to load page, status code: ${resp.statusCode}`))
    
                const bufChunks: Buffer[] = []
                resp.on('data', chunk => {
                    bufChunks.push(chunk)
                })
                resp.on('end', () => {
                    let data = Buffer.concat(bufChunks)
                    resolve(data)
                })
            })
            req.on('error', err => {
                reject(err)
            })
        })
    }
}

export default client
