// TODO: setup for dependency injection?
import db from '../db'

// TODO: better define user data obj
export interface User {
    uuid: string
    data: Object
}

export async function get(uuid: string): Promise<User> {
    try {
        let user = await db.query('SELECT * FROM users WHERE uuid = $1', [uuid])
        if (user.rowCount == 0) {
            throw 'Error: User not found'
        } else if (user.rowCount > 1) {
            throw 'Error: Multiple uuids, something\'s REALLY messed up'
        } else {
            let data: User = user.rows[0]
            return data
        }
    } catch (error) {
        throw error
    }
}

export async function add(user: User): Promise<void> {
    try {
        await db.query('INSERT INTO users (uuid, data) VALUES ($1, $2)', [user.uuid, user.data.toString()])
    } catch (error) {
        throw error
    }
}

// TODO: abstract 'save' and 'update' away into repo pattern
export async function update(user: User): Promise<void> {
    try {
        await db.query('UPDATE users SET data = $1 WHERE uuid = $2', [user.data.toString(), user.uuid])
    } catch (error) {
        throw error
    }
}
