import * as express from 'express'

const app = express()

app.get('/', (req, res) => {
    res.send(`
    <h1>It Works!</h1>
    <h3>Now keep working on it!!!</h3>
    `)
})

app.listen(8080, () => {
    console.log('App running on port 8080!')
})
