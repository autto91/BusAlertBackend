import axios from 'axios'
import * as fs from 'fs'
import { IBigBlueUglyResp, IBigBlueRoute, IBigBlueStop, IBigBlueTrip, IBigBlueStopTime } from '../types'

module bigBlueClient {
    const baseUrl = 'http://gtfs.bigbluebus.com/parsed'

    // TODO: better way of getting routes and stops?
    export async function getAllRoutes(): Promise<IBigBlueRoute[]> {
        try {
            let url = `${baseUrl}/routes.json`
            let routes = await bigBlueArrRequest<IBigBlueRoute>(url)

            return routes
        } catch (e) {
            console.log(e)
        }
    }

    export async function getAllStops(): Promise<IBigBlueStop[]> {
        try {
            let url = `${baseUrl}/stops.json`
            let stops = await bigBlueArrRequest<IBigBlueStop>(url)

            return stops
        } catch (e) {
            console.log(e)
        }
    }

    export async function getAllTrips(): Promise<IBigBlueTrip[]> {
        try {
            let url = `${baseUrl}/trips.json`
            let trips = await bigBlueArrRequest<IBigBlueTrip>(url)

            return trips
        } catch (e) {
            console.log(e)
        }
    }

    export async function getAllStopTimes(): Promise<IBigBlueStopTime[]> {
        try {
            let url = `${baseUrl}/stop_times.json`
            let stopTimes = await bigBlueArrRequest<IBigBlueStopTime>(url)

            return stopTimes
        } catch (e) {
            console.log(e)
        }
    }

    async function bigBlueArrRequest<T>(url: string): Promise<T[]> {
        try {
            let results: T[] = []
            let resp = await axios.get<IBigBlueUglyResp>(url)

            for (let key in resp.data) {
                results.push(resp.data[key])
            }

            return results
        } catch (e) {
            console.log(e)
        }
    }
}

export default bigBlueClient
