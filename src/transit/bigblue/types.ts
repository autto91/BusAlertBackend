export interface IBigBlueUglyResp {}

export interface IBigBlueRoute {
    route_id: string,
    route_url: string,
    route_long_name: string,
    route_desc: string,
    route_type: number,
    route_short_name: string,
    route_text_color: string,
    route_color: string,
    agency_id: string
}

export interface IBigBlueStop {
    stop_timezone: string,
    stop_id: string,
    stop_url: string,
    stop_desc: string,
    stop_lat: number,
    stop_lon: number,
    stop_code: string,
    stop_name: string,
    wheelchair_boarding: string,
    zone_id: string,
    location_type: number,
    parent_station: string
}

export interface IBigBlueTrip {
    route_id: string,
    trip_headsign: string,
    block_id: string,
    bikes_allowed: string,
    direction_id: string,
    wheelchair_accessible: string,
    service_id: string,
    trip_id: string,
    shape_id: string,
    trip_short_name: string
}

export interface IBigBlueStopTime {
    pickup_type: number,
    arrival_time: string,
    stop_sequence: number,
    stop_id: string,
    timepoint: number,
    drop_off_type: number,
    stop_headsign: string,
    trip_id: string,
    shape_dist_traveled: number,
    departure_time: string
}
