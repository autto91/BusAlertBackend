import axios from 'axios'
import { IMetroAllRoutes, IMetroRoute, IMetroAllRouteStops, IMetroStop } from '../types'

module metroClient {
    const baseUrl = 'http://api.metro.net/agencies'

    export async function getAllRoutes(): Promise<IMetroRoute[]> {
        try {
            let routes: IMetroRoute[] = []

            let busResp = await axios.get<IMetroAllRoutes>(`${baseUrl}/lametro/routes`)
            let railResp = await axios.get<IMetroAllRoutes>(`${baseUrl}/lametro-rail/routes`)

            routes.concat(busResp.data.items)
            routes.concat(railResp.data.items)

            return routes
        } catch (e) {
            console.log(`Error retrieving metro routes, got: ${e}`)
        }
    }

    export async function getRouteStops(route: string, agency: string): Promise<IMetroStop[]> {
        try {
            let url = `${baseUrl}/${agency}/routes/${route}/stops`
            let resp = await axios.get<IMetroAllRouteStops>(url)

            return resp.data.items

        } catch (e) {
            console.log(e)
        }
    }
}

export default metroClient
