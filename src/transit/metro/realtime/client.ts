import axios from 'axios'
import { IMetroStopPredictions, IMetroPrediction } from '../types'
import * as fs from 'fs'

module metroRtClient {
    const baseUrl = 'http://api.metro.net/agencies'

    export async function getStopPredictions(stopId: string, agency: string): Promise<IMetroPrediction[]> {
        try {
            let url = `${baseUrl}/${agency}/stops/${stopId}/predictions`
            let resp = await axios.get<IMetroStopPredictions>(url)

            return resp.data.items
        } catch (e) {
            console.log(e)
        }
    }

}

export default metroRtClient
