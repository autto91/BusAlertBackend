export interface IMetroAllRoutes {
    items: IMetroRoute[]
}

export interface IMetroRoute {
    id: string
    display_name: string
}

export interface IMetroAllRouteStops {
    items: IMetroStop[]
}

export interface IMetroStop {
    logitude: number,
    id: string,
    display_name: string,
    latitude: number
}

export interface IMetroStopPredictions {
    items: IMetroPrediction[]
}

export interface IMetroPrediction {
    minutes: number,
    seconds: number,
    block_id: string,
    routes_id: string,
    is_departing: boolean,
    run_id: string
}
