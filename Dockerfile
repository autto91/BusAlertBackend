FROM node:alpine

ADD . /app

ENV NODE_ENV=production

RUN cd /app && \
npm install

ENTRYPOINT ./app/start.sh
