CREATE TABLE users (
    id SERIAL PRIMARY KEY,
    uuid TEXT,
    data JSONB
);
